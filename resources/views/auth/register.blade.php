@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 auth-left">
            <div class="row auth-form mb-4">
                <div class="col-12 col-sm-12">
                    <div class="auth-text-top mb-4">
                        <h1>Create Account</h1>
                        <small>Already have an account? <a href="{{url('/login')}}">Login to Account</a></small>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <label for="username">Name</label>
                            <div class="input-icon">
                                <i class="mdi mdi-account"></i>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Enter Your Name">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <div class="input-icon">
                                <i class="mdi mdi-email"></i>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter Your Email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <div class="input-icon">
                                <i class="mdi mdi-lock"></i>
                                <span class="passtoggle mdi mdi-eye toggle-password"></span>
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="Enter Your Password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password</label>
                            <div class="input-icon">
                                <i class="mdi mdi-lock"></i>
                                <span class="passtoggle mdi mdi-eye toggle-password"></span>
                                <input type="password" class="form-control" id="password-confirm" name="password_confirmation"
                                       placeholder="Re-Enter-Password">
                            </div>
                        </div>
                        <div class="d-flex form-check">
                            <input type="checkbox" class="filter" id="remember" checked>
                            <label for="remember">I Accept <a href="#">Terms and Conditions</a></label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-c mt-4 mb-4">{{ __('Register') }}</button>
                    </form>
                    <p class="divider">Or</p>
                    <div class="btn-social mb-4">
                        <a href="{{ url('/login/facebook') }}"> <button class="btn bg-primary"><i class="mdi mdi-facebook"></i></button></a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ url('/login/google') }}"><button class="btn bg-danger"><i class="mdi mdi-google"></i></button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 auth-right d-lg-flex d-none bg-gradient" id="particles">
            <div class="logo">
                <img src="{{asset('assets/img/logo.png')}}" width="100" alt="logo">
            </div>
            <div class="heading">
                <h3>Welcome to AdventureNX</h3>
            </div>
            <div class="shape"></div>
        </div>
    </div>
</div>
@endsection